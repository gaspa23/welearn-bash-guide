﻿Comandi bash, su file:

1) cat

It can be used for the following purposes under UNIX or Linux.

Display text files on screen
Copy text files
Combine text files
Create new text files

TRADUZIONE:
Può essere utilizzato per i seguenti scopi in UNIX o Linux.

Visualizza file di testo sullo schermo
Copia file di testo
Combina file di testo
Crea nuovi file di testo

Example:
cat filename
cat file1 file2 
cat file1 file2 > newcombinedfile
cat < file1 > file2 #copy file1 to file2

2) diff

Compares files, and lists their differences.

Example:
diff filename1 filename2

TRADUZIONE:
Confronta i file ed elenca le loro differenze.
diff nomefile1 nomefile2


3) find

Find files in directory
find directory options pattern

TRADUZIONE:
Trova i file nella directory
trova il modello delle opzioni di directory

Example:
$ find . -name README.md
$ find /home/user1 -name '*.png'



SCRIPT

#!/bin/bash
echo '******* Clendario Anno*******'
#find $1
#grep $1
echo $1



ESERCITAZIONI, CICLO FOR, NUMERO FATTORIALE DI 5

#!/bin/bash

#a=1
#let b=a+1
#echo $b

#a=3
#b=5
#let som=a+b
#echo $som

#let som=$1+$2
#echo $som

#let som=$1*$2
#let som+=7
#echo $som

#progetto 62
#doppio
#for numero in $*
#do
#	let numero*=2
#	echo $numero
#done

#fattoriale
som=1

for numero in {1..5}
do
	let pro=1*numero
	let som=som*pro
done
echo $som