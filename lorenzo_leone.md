Comandi:
----------------------
cp
----------------------
Copies a file from one location to other.

cp filename1 filename2

Where filename1 is the source path to the file and filename2 is the destination
 path to the file.
-----------------------
diff
-----------------------
Compares files, and lists their differences.

diff filename1 filename2
-----------------------
file
-----------------------
Determine file type.

file filename

Example:

$ file index.html
 
index.html: HTML document, ASCII text

-----------------------
gzip
-----------------------
Compresses files.

gzip filename

-----------------------
gunzip
-----------------------
Un-compresses files compressed by gzip.

-----------------------
gunzip filename
-----------------------
find

Find files in directory
find directory options pattern

Example:
$ find . -name README.md
$ find /home/user1 -name '*.png'
-----------------------
ls
-----------------------
Lists your files.
-----------------------
mv
-----------------------
Moves a file from one location to other.

mv filename1 filename2

Where filename1 is the source path to the file and filename2 is the destination path to the file.

Also it can be used for rename a file.

mv old_name new_name
------------------------

ScriptOperatori

#!/bin/bash

#Es.1

a=1
let b=a+1
echo $b

#Es.2

a=3
b=5
let som=a+b
echo $som

#Es.3

grep $1 $2
let som=$1+$2
echo $som

#Es.4

som=1
for num in {1..5}
do 
let prod=1*num
let som=som*prod
done
echo $som

#Es.5

#triangolo isoscele
echo 'Area triangolo isoscele'
grep $b $h
let area=$b*$h/2
echo $area


