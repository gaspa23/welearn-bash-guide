`-gzip`


comprime i file in estenzione .zip.

es:
*`gzip` filename*

---------------------------------------------------------------

`-mv`

sposta un file da una cartella ad un altra.

es:
*`mv` filename1 filename2*

---------------------------------------------------------------

`-chown`


questo comando permette di cambiare il propretario di un file
o cartella.

es:
*`chown` -options user:group filename*

---------------------------------------------------------------

**questo è uno script bash per visualizzare il calendario dell'anno inserito**

#!/bin/bash

echo '********calendario anno********'


cal $1

---------------------------------------------------------------

Script ciclo for per calcolare numeri fattoriali


#!/bin/bash

#a=1
#let b=a+1
#echo $b

#a=3
#b=5
#let som=a+b
#echo $som

#let som=$1+$2
#echo $som

#let som=$1*$2
#let som+=7
#echo $som

#progetto 62
#doppio
#for numero in $*
#do
#	let numero*=2
#	echo $numero
#done

#fattoriale
som=1

for numero in {1..5}
do
	let pro=1*numero
	let som=som*pro
done
echo $som

